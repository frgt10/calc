﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    /// <summary>
    /// Класс, отвечающий за внутреннюю память калькулятора
    /// </summary>
    public static class CalculateMemory
    {
        /// <summary>
        /// Результат последнего вычисления
        /// </summary>
        public static double? Memory; 

        /// <summary>
        /// Текущее число - число, которое участвует в операциях в данный момент
        /// </summary>
        private static string CurrentNumber;

        /// <summary>
        /// Возвращает текущее число
        /// </summary>
        /// <returns></returns>
        public static double? GetCurrentNumber()
        {
            try
            {
                return Convert.ToDouble(CurrentNumber);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Добавляет символ к текущему числу
        /// </summary>
        /// <param name="symbol"></param>
        public static void CurrentNumberAdd(char symbol)
        {
            CurrentNumber += symbol;
        }

        /// <summary>
        /// Очищает текущее число
        /// </summary>
        public static void CurrentNumberClear()
        {
            CurrentNumber = "";
        }
    }
}

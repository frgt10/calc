﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calc
{
    public partial class MainWindow : Window
    {
        enum Action
        {
            Add,
            Minus,
            Multi,
            Div,
            Sqrt,
            Percent,
            Pow
        }
        Action? lastAction;

        public MainWindow()
        {
            InitializeComponent();            
        }

        #region Numbers And Inputs

        private void OneBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "1";
            CalculateMemory.CurrentNumberAdd('1');
        }

        private void TwoBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "2";
            CalculateMemory.CurrentNumberAdd('2');
        }

        private void ThreeBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "3";
            CalculateMemory.CurrentNumberAdd('3');
        }

        private void FourBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "4";
            CalculateMemory.CurrentNumberAdd('4');
        }

        private void FiveBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "5";
            CalculateMemory.CurrentNumberAdd('5');
        }

        private void SixBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "6";
            CalculateMemory.CurrentNumberAdd('6');
        }

        private void SevenBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "7";
            CalculateMemory.CurrentNumberAdd('7');
        }

        private void EightBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "8";
            CalculateMemory.CurrentNumberAdd('8');
        }

        private void NineBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "9";
            CalculateMemory.CurrentNumberAdd('9');
        }

        private void ZeroBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "0";
            CalculateMemory.CurrentNumberAdd('0');
        }

        private void PointBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += ",";
            CalculateMemory.CurrentNumberAdd(',');
        }

#endregion

        #region Actions
        private void PlusBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "+";
            CheckMemory();
            // если было не завершенно действие (даже само же сложение), то его нужно выполнить
            if((lastAction != null) || (CalculateMemory.GetCurrentNumber() != null))
                Count();
            lastAction = Action.Add;
        }

        private void MinusBut_Click(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text += "-";
            CheckMemory();
            if ((lastAction != null) || (CalculateMemory.GetCurrentNumber() != null))
                Count();
            lastAction = Action.Minus;
        }

        private void MultiBut_Click(object sender, RoutedEventArgs e)
        {
                          
        }

        private void DivBut_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void SqrtBut_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void PercentBut_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void PowBut_Click(object sender, RoutedEventArgs e)
        {
           
        }
        #endregion

        #region EditInput
        private void DelBut_Click(object sender, RoutedEventArgs e)
        {
                         
        }

        private void ClearBut_Click(object sender, RoutedEventArgs e)
        {
           
        }
        #endregion

        /// <summary>
        /// Если память пуста, то в неё заносится первое число
        /// и очищается текущее
        /// </summary>
        public void CheckMemory()
        {
            if (CalculateMemory.Memory == null)
            {
                CalculateMemory.Memory = Convert.ToDouble(CalculateMemory.GetCurrentNumber());
                CalculateMemory.CurrentNumberClear();
            }
        }

        private void EqualBut_Click(object sender, RoutedEventArgs e)
        {
            AnswerLabel.Content = CalculateMemory.Memory;
            Count();
            InputTextBox.Clear();
        }
        
        public void SetAnswer()
        {
            AnswerLabel.Content = CalculateMemory.Memory;
            CalculateMemory.CurrentNumberClear();
        }

        public void Count()
        {
            switch (lastAction)
            {
                case Action.Add:
                    Operation.Add();
                    break;
                case Action.Minus:
                    Operation.Minus();
                    break;
            }
            lastAction = null;
            SetAnswer();
        }
    }
}

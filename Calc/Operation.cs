﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    /// <summary>
    /// Класс, содержащий в себе операции калькулятора
    /// </summary>
    public static class Operation
    {
        /// <summary>
        /// Складывает результат последнего вычисления и текущее число
        /// </summary>
        public static void Add()
        {
            CalculateMemory.Memory = (CalculateMemory.Memory?? 0) + CalculateMemory.GetCurrentNumber();
        }

        /// <summary>
        /// Вычитает из результата последнего вычисления текущее число
        /// </summary>
        public static void Minus()
        {
            CalculateMemory.Memory = (CalculateMemory.Memory ?? 0) - CalculateMemory.GetCurrentNumber();           
        }

        /// <summary>
        /// Умножает результат последнего вычисления и текущее число
        /// </summary>
        public static void Multi()
        {
            
        }

        /// <summary>
        /// Делит результат последнего вычисления и текущее число
        /// </summary>
        public static void Div()
        {
            
        }

        /// <summary>
        /// Извлекает корень из текущего числа или результата последнего вычисления
        /// </summary>
        public static void Sqrt()
        {
            
        }

        /// <summary>
        /// Берет заданный процент из текущего числа или резальтата последнего вычисления
        /// </summary>
        public static void Percent()
        {
            
        }

        /// <summary>
        /// Возводит в заданную степень текущее число или результат последнего вычисления
        /// </summary>
        public static void Pow()
        {
            
        }
    }
}
